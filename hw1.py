import numpy
import urllib
import scipy.optimize
import random
import matplotlib.pyplot as plt

def parseData(fname):
  for l in urllib.urlopen(fname):
    yield eval(l)
    
SAMPLE_SIZE = 50000

print "Reading data..."
data = list(parseData("data/beer_50000.json"))
print "done"

data_ = data[:]

data = [d for d in data_ if d.has_key('beer/ABV')]

def feature(datum):
  feat = [1]
  feat.append(datum['beer/ABV'])
  return feat

X = [feature(d) for d in data]
y = [d['review/taste'] for d in data]
theta,residuals,rank,s = numpy.linalg.lstsq(X, y)

residuals/SAMPLE_SIZE
theta

def nonLinearFeature(datum, degree):
  feat = [1]
  for i in range(degree):
    feat.append(datum['beer/ABV']**(i+1))
  return feat

def mse(y, X, theta):
    y_predicted = numpy.dot(X, theta.T)
    sum = 0
    for i in range(len(y)):
      sum = sum + (y[i] - y_predicted[i]) ** 2
    return sum/len(X)
  
for i in range(5):
  X = [nonLinearFeature(d, i+1) for d in data]
  theta,residuals,rank,s = numpy.linalg.lstsq(X,y)
  print "residual for", (i+1), "features", residuals/SAMPLE_SIZE
  print "theta for", (i+1), "feature", theta
  mse_calculated = mse(y, X, theta)
  print "calculated mse", mse_calculated
  print "============================================"

#Question 3 of week 1
train = data[:25000]
test = data[25000:]
y_train = y[:25000]
y_test = y[25000:]

for i in range(5):
  X = [nonLinearFeature(d, i+1) for d in train]
  theta,residuals,rank,s = numpy.linalg.lstsq(X,y_train)
  print "residual for", (i+1), "features", residuals/len(X)
  print "theta for", (i+1), "feature", theta
  mse_calculated = mse(y_train, X, theta)
  print "calculated mse for training data", mse_calculated
  X_test = [nonLinearFeature(d, i+1) for d in test]
  mse_calculated = mse(y_test, X_test, theta)
  print "calculated mse for test data", mse_calculated
  print "============================================"
